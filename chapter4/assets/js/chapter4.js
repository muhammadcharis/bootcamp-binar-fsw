class Games{
    static listChoices = ['rock','scissors','paper'];
    #playerChoice = null
    #comChoice = null;

    set playerChoice(value)
    {
      this.#playerChoice = value;
    }

    get playerChoice()
    {
      return this.#playerChoice;
    }

    set comChoice(value)
    {
      this.#comChoice = value;
    }

    get comChoice()
    {
      return this.#comChoice
    }

    #rules()
    {
      if(this.#playerChoice === this.#comChoice ) return `draw`;
      else if(this.#playerChoice === 'rock' && this.#comChoice === 'scissors') return `player 1 win`;
      else if(this.#playerChoice === 'scissors' && this.#comChoice === 'paper') return `player 1 win`;
      else if(this.#playerChoice === 'paper' && this.#comChoice === 'rock') return `player 1 win`;
      else return `com win`;
    }

    matchResult()
    {
      return this.#rules();
    }
  }

  const suitGame = new Games();

  const comChoices = document.querySelectorAll(".com-choice");
  const pChoices = document.querySelectorAll(".p-choice");
  let randomCom;
  
  function startComputerRandom()
  {
    randomCom = setInterval(function (){
      let random = Math.floor(Math.random() * Games.listChoices.length);
      suitGame.comChoice = comChoices[random].getAttribute('data-value');
  },100);
  }
  

  pChoices.forEach(el => {
    el.addEventListener("click",function()
    {
        suitGame.playerChoice = this.getAttribute('data-value');
        drawSelected(suitGame.playerChoice,pChoices)
        clearInterval(randomCom);
        drawSelected(suitGame.comChoice,comChoices);

        console.clear();
        console.log(`player choose ${suitGame.playerChoice}`);
        console.log(`com choose ${suitGame.comChoice}`);
        console.log(`result game: ${suitGame.matchResult()}`);

        let blockUi = document.createElement('div');
        blockUi.classList.add('block-ui');
        
        document.body.appendChild(blockUi);

        let matchResultPanel = document.getElementById('match-section');
        let matchResult = document.createElement('div');
        matchResult.classList.add('match-result');
        matchResult.innerHTML = suitGame.matchResult();

        matchResultPanel.appendChild(matchResult);
        document.getElementById('refresh').classList.remove('d-none');
    });
  });
  
  function drawSelected (value,lists)
  {
      for (const p of lists) {
          
         if(p.getAttribute('data-value') == value)
          {
            p.style.backgroundColor = "#C4C4C4";
            p.style.borderRadius = "20px";
          }else 
          {
            p.style.backgroundColor =  "transparent";
          }
      }
  }


  document.getElementById('refresh').addEventListener("click",function()
  {
    this.classList.add('d-none');
    const blockUi = document.getElementsByClassName('block-ui')[0];
    const matchResult = document.getElementsByClassName('match-result')[0];
    const matchResultPanel = document.getElementById('match-section');

    document.body.removeChild(blockUi)
    matchResultPanel.removeChild(matchResult)
    startComputerRandom();
  });


  window.onload = () => {
    startComputerRandom();
  };