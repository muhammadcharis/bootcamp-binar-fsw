/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}", "./views/*.ejs", "./views/admin/*.ejs"],
  theme: {
    extend: {
      fontFamily: {
        openSans: "Open Sans",
        babasNeue: "Bebas Neue",
      },
      backgroundImage: {
        "games-section": "url('/assets/images/the-games-bg.jpg')",
        "features-section": "url('/assets/images/features.jpg')",
        "sysreq-section": "url('/assets/images/requirements.png')",
        "topscore-section": "url('/assets/images/top-score-bg.png')",
        "newslatter-section": "url('/assets/images/skull.png')",
      },
    },
  },
  plugins: [],
};
