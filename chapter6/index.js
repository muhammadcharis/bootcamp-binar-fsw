const express = require("express");
const path = require("path");
const fs = require("fs");
const userRoute = require("./routes/user.js");
const gameRoute = require("./routes/game.js");
const adminRoute = require("./routes/admin.js");

const app = express();
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  }),
);

app.set("view engine", "ejs");
app.set("/views", path.join(__dirname, "views"));
app.use("/assets", express.static("assets"));

app.use("/user", userRoute);
app.use("/game", gameRoute);
app.use("/admin", adminRoute);

app.get("/", function (req, res) {
  res.render("index", { query: req.query });
});

const checkAndCreateForJsonFile = (fileName) => {
  fs.open("/files/" + fileName, "r", function (err, fd) {
    if (err) {
      fs.writeFile(fileName, "[]", function (callback) {
        console.log(`The ${fileName} file is created!`);
      });
    }
  });
};

// ini untuk landing page nya
app.get("/", function (req, res) {
  res.render("index", { query: req.query });
});

app.listen(3000, () => {});
