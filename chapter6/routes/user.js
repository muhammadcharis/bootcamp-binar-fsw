const express = require("express");
const fs = require("fs");

const app = express();
app.use(express.json());
const router = express.Router();

router.post("/register", (req, res, next) => {
  const payload = req.body;

  if (!payload.name || !payload.username || !payload.password) {
    res.sendStatus(400).send("Name, username, and password is required");
  }

  let userFile = JSON.parse(fs.readFileSync("users.json"));

  const isUserExists = userFile.find(
    (el) => el.username == payload.username.toLowerCase(),
  );

  if (isUserExists) {
    res.status(409).json({
      message: "user already exists",
      code: "409",
    });
  } else {
    userFile.push({
      name: payload.name,
      username: payload.username.toLowerCase(),
      password: payload.password,
    });

    fs.writeFileSync("files/users.json", JSON.stringify(userFile));

    res.json({
      name: payload.name,
      username: payload.username.toLowerCase(),
      password: payload.password,
    });
  }
});

router.post("/login", (req, res, next) => {
  const payload = req.body;

  if (!payload.username || !payload.password) {
    res.sendStatus(400).json({
      message: "Name, username, and password is required",
      code: 400,
    });
  }

  let userFile = JSON.parse(fs.readFileSync("files/users.json"));

  const isUserExists = userFile.find(
    (el) =>
      el.username == payload.username.toLowerCase() &&
      el.password == payload.password,
  );

  if (isUserExists) {
    fs.writeFileSync("files/session.json", JSON.stringify(isUserExists));
    res.sendStatus(200);
  } else {
    res.sendStatus(401).json({
      message: "wrong password",
      code: 401,
    });
  }
});

router.post("/logout", (req, res, next) => {
  fs.writeFileSync("files/session.json", "[{}]");
  res.sendStatus(200);
});

module.exports = router;
