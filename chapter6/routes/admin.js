const express = require("express");
const fs = require("fs");
const path = require("path");
const { UserGames, UserGameBiodata, UserGameHistory } = require("../models");
const { Op } = require("sequelize");

const router = express.Router();

router.use(function (req, res, next) {
  let sessionFile = JSON.parse(fs.readFileSync("files/session.json"));
  const isAdminLogin = sessionFile.find((el) => el.isAdmin == true);

  if (!isAdminLogin) {
    if (req.originalUrl != "/admin/login") {
      res.redirect("/admin/login");
      return;
    }
  } else {
    if (["/admin/login", "/admin"].includes(req.originalUrl)) {
      res.redirect("/admin/dashboard");
      return;
    }
  }
  next();
});

router.get("/login", function (req, res) {
  res.render("admin/login");
});

router.get("/dashboard", async (req, res) => {
  const users = await UserGames.findAll({
    include: [
      {
        model: UserGameBiodata,
        as: "UserGameBiodata",
        attributes: [
          "firstName",
          "lastName",
          "userGameId",
          "phoneNumber",
          "address",
        ],
      },
    ],
    attributes: ["username", "id"],
    order: [["id", "DESC"]],
  });

  res.render("admin/dashboard", { dataUser: users });
});

router.get("/user", async (req, res) => {
  const users = await UserGames.findAll({
    include: [
      {
        model: UserGameBiodata,
        as: "UserGameBiodata",
        attributes: [
          "firstName",
          "lastName",
          "userGameId",
          "phoneNumber",
          "address",
        ],
      },
    ],
    attributes: ["username", "id"],
    order: [["id", "DESC"]],
  });
  res.render("admin/user", { dataUser: users });
});

router.get("/edit-user/:id", async (req, res) => {
  const params = req.params;
  const user = await UserGames.findByPk(params.id, {
    include: [
      {
        model: UserGameBiodata,
        as: "UserGameBiodata",
        attributes: [
          "firstName",
          "lastName",
          "userGameId",
          "phoneNumber",
          "address",
        ],
      },
    ],
    attributes: ["username", "id"],
    order: [["id", "DESC"]],
  });

  if (user) {
    res.status(200).json({
      message: user,
      code: "200",
    });
  }
});

router.post("/logout", (req, res) => {
  fs.writeFileSync("files/session.json", JSON.stringify([]));
  res.redirect("/admin/login");
});

router.post("/store-user", async (req, res) => {
  const payload = req.body;
  let flag;
  await UserGames.count({
    where: { username: payload.username },
  }).then((count) => {
    if (count == 0) {
      UserGames.create(
        {
          username: payload.username,
          password: payload.password,
          UserGameBiodata: {
            firstName: payload.firstName,
            lastName: payload.lastName,
            phoneNumber: payload.phoneNumber,
            address: payload.address,
          },
        },
        {
          include: [
            {
              model: UserGameBiodata,
              as: "UserGameBiodata",
            },
          ],
        },
      );
      flag = true;
    } else {
      flag = false;
    }
  });

  if (!flag)
    res.status(409).json({
      message: "username already exists",
      code: "409",
    });
  else
    res.status(200).json({
      message: "insert successfully",
      code: "200",
    });
});

router.put("/update-user/:id", async (req, res) => {
  const params = req.params;
  const payload = req.body;
  let flag;

  await UserGames.count({
    where: {
      [Op.and]: [
        { username: payload.username },
        {
          id: {
            [Op.ne]: params.id,
          },
        },
      ],
    },
  }).then(async (count) => {
    if (count == 0) {
      await UserGames.update(
        {
          username: payload.username,
          password: payload.password,
        },
        { where: { id: params.id } },
      ).then(async (row) => {
        await UserGameBiodata.findOne({
          where: { userGameId: params.id },
        }).then(function (obj) {
          // update
          if (obj)
            return obj.update({
              userGameId: params.id,
              firstName: payload.firstName,
              lastName: payload.lastName,
              phoneNumber: payload.phoneNumber,
              address: payload.address,
            });
          // insert
          return UserGameBiodata.create({
            userGameId: params.id,
            firstName: payload.firstName,
            lastName: payload.lastName,
            phoneNumber: payload.phoneNumber,
            address: payload.address,
          });
        });
      });
      flag = true;
    } else {
      flag = false;
    }
  });

  if (!flag)
    res.status(409).json({
      message: "username already exists",
      code: "409",
    });
  else
    res.status(200).json({
      message: "update successfully",
      code: "200",
    });
});

router.delete("/user-delete", async (req, res) => {
  const payload = req.body;

  await UserGameBiodata.destroy({
    where: {
      userGameId: payload.id,
    },
  });

  await UserGameHistory.destroy({
    where: {
      userGameId: payload.id,
    },
  });

  await UserGames.destroy({
    where: {
      id: payload.id,
    },
  });

  res.sendStatus(200);
});

router.post("/login", function (req, res) {
  const payload = req.body;

  if (!payload.username || !payload.password) {
    res.sendStatus(400).json({
      message: "Username, and password is required",
      code: 400,
    });
  }

  let userFile = JSON.parse(fs.readFileSync("files/users.json"));

  const isUserExists = userFile.find(
    (el) =>
      el.username == payload.username.toLowerCase() &&
      el.password == payload.password &&
      el.isAdmin == true,
  );

  if (isUserExists) {
    fs.writeFileSync("files/session.json", JSON.stringify([isUserExists]));
    res.redirect("/admin/dashboard");
  } else {
    res.redirect("/admin/login");
  }
});

module.exports = router;
