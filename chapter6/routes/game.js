const express = require("express");
const fs = require("fs");

const router = express.Router();

router.use(function (req, res, next) {
  try {
    let sessionFile = JSON.parse(fs.readFileSync("files/session.json"));
    if (sessionFile) {
      next();
    } else {
      res.redirect("/?flag=error_login");
    }
  } catch (error) {
    res.redirect("/?flag=error_login");
  }
});

router.get("/", function (req, res) {
  res.render("game");
});

router.post("/save-result", function (req, res) {
  const payload = req.body;
  let sessionFile = JSON.parse(fs.readFileSync("files/session.json"));
  let resultFile = JSON.parse(fs.readFileSync("files/games.json"));

  resultFile.push({
    username: sessionFile.username,
    result: payload.result.toLowerCase(),
  });

  fs.writeFileSync("files/games.json", JSON.stringify(resultFile));

  res.json({
    message: "insert successfully",
  });
});

module.exports = router;
