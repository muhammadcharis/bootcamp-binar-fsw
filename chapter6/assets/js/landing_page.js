const modalOverlay = document.getElementById("modal-overlay");
const btnSwitch = document.getElementById("btn-switch");
const btnLoginSwitch = document.getElementById("btn-login-switch");
const btnRegisSwitch = document.getElementById("btn-regis-switch");
const btnGettingStarted = document.getElementById("btnGettingStarted");
const btnCloseLogin = document.getElementsByClassName("btn-close-login");
const modalLogin = document.getElementById("loginModal");
const signUpForm = document.getElementById("signup-form");
const signInForm = document.getElementById("signin-form");
const query = JSON.parse(document.getElementById("EJSData").value);

function openModal(val) {
  if (val) {
    modalOverlay.classList.remove("hidden");

    setTimeout(() => {
      modalLogin.classList.remove("opacity-0");
      modalLogin.classList.remove("scale-150");
      modalLogin.classList.remove("-translate-y-full");
    }, 100);
  } else {
    modalLogin.classList.add("-translate-y-full");
    setTimeout(() => {
      modalLogin.classList.add("opacity-0");
      modalLogin.classList.add("scale-150");
    }, 100);
    setTimeout(() => modalOverlay.classList.add("hidden"), 300);
  }
}

btnGettingStarted.onclick = () => openModal(true);

for (let i = 0; i < btnCloseLogin.length; i++) {
  btnCloseLogin[i].onclick = () => openModal(false);
}

btnLoginSwitch.addEventListener("click", function () {
  btnSwitch.classList.remove("right-button");
  btnSwitch.classList.add("left-button");
  signInForm.classList.remove("hidden");
  signUpForm.classList.add("hidden");
});

btnRegisSwitch.addEventListener("click", function () {
  btnSwitch.classList.add("right-button");
  btnSwitch.classList.remove("left-button");
  signUpForm.classList.remove("hidden");
  signInForm.classList.add("hidden");
});

window.onload = () => {
  if (typeof query.flag !== "undefined") {
    document.getElementById("error_message").innerText = "Please login first";
    document.getElementById("error").classList.remove("hidden");

    setTimeout(() => {
      document.getElementById("error").classList.add("hidden");
    }, 3000);
  } else document.getElementById("error").classList.add("hidden");
};

document
  .getElementById("signup-form")
  .addEventListener("submit", function (event) {
    event.preventDefault();

    const username = document.getElementById("regis_username").value;
    const name = document.getElementById("regis_name").value;
    const password = document.getElementById("regis_password").value;
    const url = this.action;

    fetch(url, {
      method: "POST",
      body: JSON.stringify({
        username: username,
        name: name,
        password: password,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then(function (response) {
        document.getElementById("regis_username").value = null;
        document.getElementById("regis_name").value = null;
        document.getElementById("regis_password").value = null;

        return response.json();
      })
      .then(function (data) {
        if (data.code == 409) {
          document.getElementById("error_message").innerText = data.message;
          document.getElementById("error").classList.remove("hidden");

          setTimeout(() => {
            document.getElementById("error").classList.add("hidden");
          }, 3000);
        } else {
          console.log(data);
        }
      });
  });

document
  .getElementById("signin-form")
  .addEventListener("submit", function (event) {
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;
    const url = this.action;

    fetch(url, {
      method: "POST",
      body: JSON.stringify({
        username: username,
        password: password,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    }).then(function (data) {
      location.replace("/game");
    });
  });
