"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class UserGame extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.UserGameBiodata, {
        foreignKey: "userGameId",
        as: "UserGameBiodata",
        onDelete: "CASCADE",
      });

      this.hasMany(models.UserGameHistory, {
        foreignKey: "userGameId",
        as: "UserGameHistories",
        onDelete: "CASCADE",
      });
    }
  }
  UserGame.init(
    {
      username: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "UserGames",
      freezeTableName: true,
      tableName: "UserGames",
      //underscored: true,
    },
  );
  return UserGame;
};
