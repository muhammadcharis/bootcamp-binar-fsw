"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class UserGameHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.UserGames, {
        foreignKey: "id",
        as: "UserGame",
      });
    }
  }
  UserGameHistory.init(
    {
      userGameId: DataTypes.INTEGER,
      matchResult: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "UserGameHistory",
      //underscored: true,
      freezeTableName: true,
      tableName: "UserGameHistories",
    },
  );
  return UserGameHistory;
};
