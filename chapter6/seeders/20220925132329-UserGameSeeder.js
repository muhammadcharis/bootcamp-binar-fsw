"use strict";
const { faker } = require("@faker-js/faker");

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    let DataUsers = new Array();
    let user = new Object({
      username: "admin",
      password: "admin",
      is_admin: true,
      created_at: new Date(),
      updated_at: new Date(),
    });

    DataUsers.push(user);

    for (let index = 0; index < 100; index++) {
      let user2 = new Object({
        username: faker.internet.userName(),
        password: faker.internet.password(8),
        is_admin: false,
        created_at: new Date(),
        updated_at: new Date(),
      });

      DataUsers.push(user2);
    }

    //console.log(DataUsers.length);
    await queryInterface.bulkInsert("UserGames", DataUsers, {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
