"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Rooms", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: {
        type: Sequelize.STRING,
      },
      UserGameId1: {
        type: Sequelize.INTEGER,
        onDelete: "CASCADE",
        references: {
          model: "UserGames",
          key: "id",
        },
      },
      UserGameId2: {
        type: Sequelize.INTEGER,
        onDelete: "CASCADE",
        references: {
          model: "UserGames",
          key: "id",
        },
      },
      UserGame1Hand: {
        type: Sequelize.ARRAY(Sequelize.TEXT),
      },
      UserGame2Hand: {
        type: Sequelize.ARRAY(Sequelize.TEXT),
      },
      ResultArray: {
        type: Sequelize.ARRAY(Sequelize.TEXT),
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Rooms");
  },
};
