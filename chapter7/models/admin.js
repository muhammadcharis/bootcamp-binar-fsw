"use strict";
const { Model } = require("sequelize");
const bcrypt = require("bcrypt");

module.exports = (sequelize, DataTypes) => {
  class Admin extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    static #encrypt = (password) => bcrypt.hashSync(password, 10);
    static register = ({ username, password }) => {
      const encryptedPassword = this.#encrypt(password);
      return this.create({ username, password: encryptedPassword });
    };

    static authenticate = async (username, password) => {
      try {
        const admin = await this.findOne({
          where: sequelize.where(
            sequelize.fn("lower", sequelize.col("username")),
            sequelize.fn("lower", username),
          ),
        });
        if (!admin) return Promise.reject({ message: "user not found !" });

        const isPasswordValid = bcrypt.compareSync(password, admin.password);
        if (!isPasswordValid)
          return Promise.reject({ message: "password invalid !" });

        return Promise.resolve(admin);
      } catch (error) {
        return Promise.reject({ message: "database error !" });
      }
    };
  }
  Admin.init(
    {
      username: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      freezeTableName: true,
      modelName: "Admin",
      tableName: "Admins",
    },
  );
  return Admin;
};
