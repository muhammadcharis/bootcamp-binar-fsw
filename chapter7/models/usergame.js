"use strict";
const { Model } = require("sequelize");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
require("dotenv").config();

module.exports = (sequelize, DataTypes) => {
  class UserGame extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.UserGameBiodata, {
        foreignKey: "userGameId",
        as: "UserGameBiodata",
        onDelete: "CASCADE",
      });

      this.hasMany(models.UserGameHistory, {
        foreignKey: "userGameId",
        as: "UserGameHistories",
        onDelete: "CASCADE",
      });
    }

    static #encrypt = (password) => bcrypt.hashSync(password, 10);
    static register = ({ username, password }) => {
      const encryptedPassword = this.#encrypt(password);
      return this.create({
        username,
        password: encryptedPassword,
      });
    };

    static generateToken = async (id, username) => {
      const payLoad = {
        id: id,
        username: username,
      };

      const accessToken = jwt.sign(payLoad, process.env.SECRET, {
        expiresIn: "1d",
      });

      const refreshToken = jwt.sign(payLoad, process.env.REFRESH_TOKEN_SECRET, {
        expiresIn: "1d",
      });

      await this.update(
        { refreshToken: refreshToken },
        {
          where: {
            id: id,
          },
        },
      );
      return { refreshToken, accessToken };
    };

    static authenticate = async (username, password) => {
      try {
        const user = await this.findOne({
          where: sequelize.where(
            sequelize.fn("lower", sequelize.col("username")),
            sequelize.fn("lower", username),
          ),
        });
        if (!user) return Promise.reject({ message: "user not found !" });

        const isPasswordValid = bcrypt.compareSync(password, user.password);
        if (!isPasswordValid)
          return Promise.reject({ message: "password invalid !" });

        return Promise.resolve(user);
      } catch (error) {
        return Promise.reject({ message: "database error !" });
      }
    };
  }
  UserGame.init(
    {
      username: DataTypes.STRING,
      password: DataTypes.STRING,
      refreshToken: DataTypes.TEXT,
    },
    {
      sequelize,
      modelName: "UserGames",
      freezeTableName: true,
      tableName: "UserGames",
    },
  );
  return UserGame;
};
