"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class UserGameBiodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.UserGames, {
        foreignKey: "id",
      });
    }
  }
  UserGameBiodata.init(
    {
      firstName: DataTypes.STRING,
      lastName: DataTypes.STRING,
      address: DataTypes.TEXT,
      phoneNumber: DataTypes.STRING,
      userGameId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "UserGameBiodata",
      //underscored: true,
      freezeTableName: true,
      tableName: "UserGameBiodata",
    },
  );
  return UserGameBiodata;
};
