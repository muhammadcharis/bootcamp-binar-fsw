"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.UserGames, {
        foreignKey: "UserGameId1",
        as: "userGameId1",
      });
      this.belongsTo(models.UserGames, {
        foreignKey: "UserGameId2",
        as: "userGameId2",
      });
    }

    static calculateResult = async (id) => {
      const room = await this.findByPk(id);
      const UserGame1Hand = room.UserGame1Hand;
      const UserGame2Hand = room.UserGame2Hand;

      const resultArray = [];
      for (let index = 0; index < UserGame1Hand.length; index++) {
        const el1 = UserGame1Hand[index];
        for (let index2 = 0; index2 < UserGame2Hand.length; index2++) {
          const el2 = UserGame2Hand[index2];
          if (index == index2) {
            let p1 = el1.toLowerCase();
            let p2 = el2.toLowerCase();
            if (p1 == p2) resultArray.push("draw");
            else if (p1 === "rock" && p2 === "scissors")
              resultArray.push("player 1 win");
            else if (p1 === "scissors" && p2 === "paper")
              resultArray.push("player 1 win");
            else if (p1 === "paper" && p2 === "rock")
              resultArray.push("player 1 win");
            else resultArray.push("player 2 win");
          }
        }
      }

      room.ResultArray = resultArray;
      await room.save();
    };
  }
  Room.init(
    {
      name: DataTypes.STRING,
      UserGameId1: DataTypes.STRING,
      UserGameId2: DataTypes.STRING,
      UserGame1Hand: DataTypes.ARRAY(DataTypes.STRING),
      UserGame2Hand: DataTypes.ARRAY(DataTypes.STRING),
      ResultArray: DataTypes.ARRAY(DataTypes.STRING),
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "Room",
      freezeTableName: true,
      tableName: "Rooms",
    },
  );
  return Room;
};
