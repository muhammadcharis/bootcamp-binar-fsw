const { UserGames, UserGameBiodata } = require("../models");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const register = async (req, res, next) => {
  const payload = req.body;
  let flag;

  await UserGames.findOne({
    where: { username: payload.username },
  }).then(async (count) => {
    if (!count) {
      await UserGames.register({
        username: payload.username,
        password: payload.password,
      }).then(async (row) => {
        await UserGameBiodata.create({
          firstName: payload.name,
          userGameId: row.id,
        });
      });
      flag = true;
    } else {
      flag = false;
    }
  });

  if (!flag)
    res.status(409).json({
      message: "username already exists",
      code: "409",
    });
  else
    res.status(200).json({
      message: "insert successfully",
      code: "200",
    });
};

const login = (req, res, next) => {
  const payload = req.body;
  if (!payload.username || !payload.password) {
    res.sendStatus(400).json({
      message: "Name, username, and password is required",
      code: 400,
    });
  } else
    UserGames.authenticate(payload.username, payload.password)
      .then(async (user) => {
        const token = await UserGames.generateToken(user.id, user.username);

        res.cookie("refreshToken", token.refreshToken, {
          httpOnly: true,
          maxAge: 24 * 60 * 60 * 1000,
          // secure: true, jika pake https
        });

        res.json({
          id: user.id,
          username: user.username,
          accessToken: token.accessToken,
        });
      })
      .catch((err) => {
        next(err);
      });
};

const refreshToken = async (req, res) => {
  try {
    const refreshToken = req.cookies.refreshToken;
    if (!refreshToken)
      res.sendStatus(401).json({
        message: "forbidden",
        code: 401,
      });
    else {
      const userGame = await UserGames.findOne({
        where: { refreshToken: refreshToken },
      });

      if (!userGame) {
        res.sendStatus(403).json({
          message: "forbidden",
          code: 403,
        });
      } else {
        jwt.verify(
          refreshToken,
          process.env.REFRESH_TOKEN_SECRET,
          (error, decoded) => {
            if (error)
              res.sendStatus(403).json({
                message: "forbidden",
                code: 403,
              });
            else {
              const payLoad = {
                id: userGame.id,
                username: userGame.username,
              };

              const accessToken = jwt.sign(payLoad, process.env.SECRET, {
                expiresIn: "1d",
              });

              res.json({
                accessToken,
              });
            }
          },
        );
      }
    }
  } catch (error) {
    console.log("error");
    console.log(error);
  }
};

const logout = async (req, res) => {
  const refreshToken = req.cookies.refreshToken;
  if (!refreshToken) {
    res.sendStatus(204).json({
      message: "not content",
      code: 204,
    });
  } else {
    const userGame = await UserGames.findOne({
      where: { refreshToken: refreshToken },
    });
    if (!userGame) {
      res.sendStatus(204).json({
        message: "not content",
        code: 204,
      });
    } else {
      userGame.refreshToken = null;
      await userGame.save();

      res.clearCookie("refreshToken");
      return res.sendStatus(200).json({
        message: "success",
        code: 200,
      });
    }
  }
};

module.exports = { register, login, refreshToken, logout };
