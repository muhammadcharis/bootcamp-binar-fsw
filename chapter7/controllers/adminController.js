const passport = require("../lib/passport");
const {
  UserGames,
  UserGameBiodata,
  UserGameHistory,
  Admin,
} = require("../models");

const dashboard = async (req, res) => {
  const users = await UserGames.findAll({
    include: [
      {
        model: UserGameBiodata,
        as: "UserGameBiodata",
        attributes: [
          "firstName",
          "lastName",
          "userGameId",
          "phoneNumber",
          "address",
        ],
      },
    ],
    attributes: ["username", "id"],
    order: [["id", "DESC"]],
  });

  res.render("admin/dashboard", { dataUser: users });
};

const index = (req, res) => {};
const login = (req, res) => {
  res.render("admin/login");
};

const admin = async (req, res) => {
  const data = await Admin.findAll({
    attributes: ["username", "id"],
    order: [["id", "DESC"]],
  });
  res.render("admin/admin", { dataUser: data });
};

const user = async (req, res) => {
  const users = await UserGames.findAll({
    include: [
      {
        model: UserGameBiodata,
        as: "UserGameBiodata",
        attributes: [
          "firstName",
          "lastName",
          "userGameId",
          "phoneNumber",
          "address",
        ],
      },
    ],
    attributes: ["username", "id"],
    order: [["id", "DESC"]],
  });
  res.render("admin/user", { dataUser: users });
};

const getDashboard = async (req, res) => {
  const users = await UserGames.findAll({
    include: [
      {
        model: UserGameBiodata,
        as: "UserGameBiodata",
        attributes: [
          "firstName",
          "lastName",
          "userGameId",
          "phoneNumber",
          "address",
        ],
      },
    ],
    attributes: ["username", "id"],
    order: [["id", "DESC"]],
  });

  res.render("admin/dashboard", { dataUser: users });
};

const getUser = async (req, res) => {
  const users = await UserGames.findAll({
    include: [
      {
        model: UserGameBiodata,
        as: "UserGameBiodata",
        attributes: [
          "firstName",
          "lastName",
          "userGameId",
          "phoneNumber",
          "address",
        ],
      },
    ],
    attributes: ["username", "id"],
    order: [["id", "DESC"]],
  });
  res.render("admin/user", { dataUser: users });
};

const editAdmin = async (req, res) => {
  const params = req.params;
  const user = await UserGames.findByPk(params.id, {
    include: [
      {
        model: UserGameBiodata,
        as: "UserGameBiodata",
        attributes: [
          "firstName",
          "lastName",
          "userGameId",
          "phoneNumber",
          "address",
        ],
      },
    ],
    attributes: ["username", "id"],
    order: [["id", "DESC"]],
  });

  if (user) {
    res.status(200).json({
      message: user,
      code: "200",
    });
  }
};

const updateAdmin = async (req, res) => {
  const params = req.params;
  const payload = req.body;
  let flag;

  await UserGames.count({
    where: {
      [Op.and]: [
        { username: payload.username },
        {
          id: {
            [Op.ne]: params.id,
          },
        },
      ],
    },
  }).then(async (count) => {
    if (count == 0) {
      await UserGames.update(
        {
          username: payload.username,
          password: payload.password,
        },
        { where: { id: params.id } },
      ).then(async (row) => {
        await UserGameBiodata.findOne({
          where: { userGameId: params.id },
        }).then(function (obj) {
          // update
          if (obj)
            return obj.update({
              userGameId: params.id,
              firstName: payload.firstName,
              lastName: payload.lastName,
              phoneNumber: payload.phoneNumber,
              address: payload.address,
            });
          // insert
          return UserGameBiodata.create({
            userGameId: params.id,
            firstName: payload.firstName,
            lastName: payload.lastName,
            phoneNumber: payload.phoneNumber,
            address: payload.address,
          });
        });
      });
      flag = true;
    } else {
      flag = false;
    }
  });

  if (!flag)
    res.status(409).json({
      message: "username already exists",
      code: "409",
    });
  else
    res.status(200).json({
      message: "update successfully",
      code: "200",
    });
};

const deleteAdmin = async (req, res) => {
  const payload = req.body;

  await UserGameBiodata.destroy({
    where: {
      userGameId: payload.id,
    },
  });

  await UserGameHistory.destroy({
    where: {
      userGameId: payload.id,
    },
  });

  await UserGames.destroy({
    where: {
      id: payload.id,
    },
  });

  res.sendStatus(200);
};

const loginAdmin = passport.authenticate("local", {
  successRedirect: "/admin/dashboard",
  failureRedirect: "/admin/login",
  failureFlash: true,
});

const logout = (req, res, next) => {
  req.logout(function (err) {
    if (err) return next(err);

    res.redirect("/admin/login");
  });
};

module.exports = {
  index,
  dashboard,
  getDashboard,
  getUser,
  editAdmin,
  updateAdmin,
  deleteAdmin,
  loginAdmin,
  admin,
  user,
  login,
  logout,
};
