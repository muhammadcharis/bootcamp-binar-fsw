const { UserGames, Room, sequelize, UserGameHistory } = require("../models");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const index = (req, res) => {
  res.render("game");
};

const storeRoom = async (req, res) => {
  const user = req.user;
  const body = req.body;

  const room = await Room.create({
    name: body.name,
    UserGameId1: user.id,
    createdAt: new Date(),
    updatedAt: new Date(),
  }).then((newRow) => {
    res.status(200).json({
      data: {
        name: newRow.name,
        UserGameId1: newRow.UserGameId1,
        createdAt: newRow.createdAt,
        updatedAt: newRow.updatedAt,
      },
      code: 200,
    });
  });
};

const roomFight = async (req, res) => {
  const params = req.params;
  const user = req.user;
  const body = req.body;

  const room = await Room.findByPk(params.id);

  if (!room) {
    res.sendStatus(409).json({
      message: "room not found",
      code: 409,
    });
  } else {
    if (room.UserGameId1 && room.UserGameId2) {
      if (room.UserGameId1 == user.id) {
        let hand1 = room.UserGame1Hand ? room.UserGame1Hand : [];
        if (hand1.length < 3) {
          hand1.push(body.hand);
          room.UserGame1Hand = sequelize.fn(
            "array_append",
            sequelize.col("UserGame1Hand"),
            body.hand,
          );
          await room.save();

          res.json({
            data: {
              message: "please fight again",
              UserGameId1: room.UserGameId1,
              UserGame1Hand: hand1,
            },
            code: 200,
          });
        } else {
          res.json({
            message: "you already 3 turn,please check result",
            code: 419,
          });
        }
      } else if (room.UserGameId2 == user.id) {
        let hand2 = room.UserGame2Hand ? room.UserGame2Hand : [];
        if (hand2.length < 3) {
          hand2.push(body.hand);
          room.UserGame2Hand = sequelize.fn(
            "array_append",
            sequelize.col("UserGame2Hand"),
            body.hand,
          );
          await room.save();

          res.json({
            data: {
              message: "please fight again",
              UserGameId2: room.UserGameId2,
              UserGame2Hand: hand2,
            },
            code: 200,
          });
        } else {
          res.json({
            message: "you already 3 turn, please check result",
            code: 419,
          });
        }
      } else {
        res.sendStatus(404);
      }

      Room.calculateResult(room.id);
    } else {
      if (room.UserGameId1 == user.id && room.UserGameId2 == null) {
        res.json({
          message: "player 2 not found",
          code: 400,
        });
      } else {
        let hand2 = room.UserGame2Hand ? room.UserGame2Hand : [];
        hand2.push(body.hand);

        room.UserGameId2 = user.id;
        room.UserGame2Hand = hand2;
        await room.save();

        res.json({
          data: {
            UserGameId2: room.UserGameId2,
            message: "please fight again",
            UserGame2Hand: room.UserGame2Hand,
          },
          code: 200,
        });
      }
    }
  }
};

const roomResult = async (req, res) => {
  const params = req.params;
  const room = await Room.findByPk(params.id);

  if (!room) {
    res.json({
      message: "room not found",
      code: 409,
    });
  } else {
    const UserGame1Hand = room.UserGame1Hand;
    const UserGame2Hand = room.UserGame2Hand;
    const ResultArray = room.ResultArray;

    if (UserGame1Hand.length < 3) {
      res.json({
        message: `player 1 still ${UserGame1Hand.length} round. please wait`,
        code: 409,
      });
    } else if (UserGame2Hand.length < 3) {
      res.json({
        message: `player 2 still ${UserGame2Hand.length} round. please wait`,
        code: 409,
      });
    } else {
      let resultP1 = 0;
      let resultP2 = 0;

      ResultArray.forEach((el) => {
        if (el == "player 2 win") resultP2++;
        else if (el == "player 1 win") resultP1++;
      });

      await UserGameHistory.create({
        userGameId: room.UserGameId1,
        roomId: room.id,
        matchResult: resultP1,
      });

      await UserGameHistory.create({
        userGameId: room.UserGameId2,
        roomId: room.id,
        matchResult: resultP2,
      });
      res.json({
        ResultArray,
        code: 200,
      });
    }
  }
};

module.exports = {
  index,
  storeRoom,
  roomFight,
  roomResult,
};
