module.exports = (req, res, next) => {
  if (req.isAuthenticated()) {
    if (req.originalUrl == "/admin") return res.redirect("/admin/dashboard");
    else if (req.originalUrl == "/admin/login")
      return res.redirect("/admin/dashboard");
    else return next();
  } else {
    if (req.originalUrl == "/admin/login") return next();
    else return res.redirect("/admin/login");
  }
};
