const jwt = require("jsonwebtoken");
require("dotenv").config();

module.exports = (req, res, next) => {
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(" ")[1]; // jika user kirim token maka akan di split tokennya.

  if (token == null) {
    return res.status(401).json({
      message: "please login first !",
      code: "401",
    });
  } else {
    jwt.verify(token, process.env.SECRET, (err, decode) => {
      if (err) {
        return res.status(403).json({
          message: "forbidden",
          code: "403",
        });
      } else {
        next();
      }
    });
  }
};
