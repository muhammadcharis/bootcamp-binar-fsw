const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const session = require("express-session");
const flash = require("express-flash");
const passport = require("./lib/passport");
const passportJWT = require("./lib/passportJWT");
const cors = require("cors");
//const axios = require("axios");
//const memoryStore = new session.MemoryStore();
require("dotenv").config();

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const adminRouter = require("./routes/admin");
const roomGameRouter = require("./routes/roomGame");

const app = express();
//app.use(axios());

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use(
  session({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: false,
    //name untuk ganti cookies name nya
    //store: memoryStore, // jika mau ambil sessionnya ( jika mau lebih mantap pake redis, sehingga apabila server di restart data tidak akan hilang)
  }),
);

app.use(flash());
app.use(passport.initialize());
app.use(passportJWT.initialize());
app.use(passport.session());
app.use(
  cors({
    credentials: true,
    origin: "http://localhost:3001",
  }),
);

// supaya tidak perlu mengirimkan credential setiap kali melakukan request

/*app.use("/session", (req, res) => {
  memoryStore.get(req.sessionID, (err, data) => {
    console.log(data);
  });
});*/

app.use("/", indexRouter);
app.use("/user", usersRouter);
app.use("/admin", adminRouter);
app.use("/room-game", roomGameRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
