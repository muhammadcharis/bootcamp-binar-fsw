const router = require("express").Router();
const roomController = require("../controllers/roomController");
const restrictToken = require("../middlewares/authToken");
//const passportJWT = require("../lib/passportJWT");

router.get("/", restrictToken, roomController.index);
router.post("/store", restrictToken, roomController.storeRoom);
router.post("/fight/:id", restrictToken, roomController.roomFight);
router.post("/result/:id", restrictToken, roomController.roomResult);

module.exports = router;
