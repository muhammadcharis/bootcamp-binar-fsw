const router = require("express").Router();
const userController = require("../controllers/userController");

/* GET users listing. */
router.get("/", function (req, res, next) {
  res.send("respond with a resource");
});

router.post("/register", userController.register);
router.post("/login", userController.login);
router.get("/refresh-token", userController.refreshToken);
router.delete("/logout", userController.logout);

module.exports = router;
