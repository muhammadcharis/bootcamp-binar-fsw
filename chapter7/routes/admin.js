const router = require("express").Router();
const adminController = require("../controllers/adminController");
const restrict = require("../middlewares/authSession");

router.get("/", restrict, adminController.index);
router.get("/admin", restrict, adminController.admin);
router.get("/user", restrict, adminController.user);
router.get("/dashboard", restrict, adminController.dashboard);
router.get("/login", restrict, adminController.login);
router.post("/login", adminController.loginAdmin);
router.post("/logout", adminController.logout);

module.exports = router;
