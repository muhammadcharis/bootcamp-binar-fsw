let navigation = document.querySelector(".navigation");
let lists = document.querySelectorAll(".navigation li");
let logout = document.getElementById("logout");
let toggle = document.getElementById("toggle");
let main = document.getElementById("main");

if (toggle) {
  toggle.addEventListener("click", function () {
    navigation.classList.toggle("active");
    main.classList.toggle("active");
  });
}

if (lists.length > 0) {
  function activeLink() {
    lists.forEach((item) => {
      item.classList.remove("hovered");
      item.classList.remove("active");
      this.classList.add("hovered");
    });
  }

  lists.forEach((item) => {
    item.addEventListener("mouseover", activeLink);
  });
}
