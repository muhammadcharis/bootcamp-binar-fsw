/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./views/**/*.ejs", "./views/*.ejs"],
  theme: {
    extend: {
      fontFamily: {
        openSans: "Open Sans",
        babasNeue: "Bebas Neue",
      },
      backgroundImage: {
        "games-section": "url('/images/the-games-bg.jpg')",
        "features-section": "url('/images/features.jpg')",
        "sysreq-section": "url('/images/requirements.png')",
        "topscore-section": "url('/images/top-score-bg.png')",
        "newslatter-section": "url('/images/skull.png')",
      },
    },
  },
  plugins: [],
};
