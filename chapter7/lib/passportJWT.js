const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");
const { UserGames } = require("../models");
const passport = require("passport");

require("dotenv").config();

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.SECRET,
};

passport.use(
  new JwtStrategy(jwtOptions, (payload, done) => {
    UserGames.findOne({ where: { id: payload.id } })
      .then((user) => {
        if (user) {
          return done(null, user);
        }
        return done(null, false);
      })
      .catch((err) => {
        console.log(err);
      });
  }),
);

module.exports = passport;
